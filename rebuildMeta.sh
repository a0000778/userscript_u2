#!/bin/bash
echo "remove *.meta.js"
rm -f *.meta.js

for file in $(ls *.user.js)
do
	name=${file:0:${#file}-8}
	echo "rebuild $name.meta.js ..."
	grep -B 9999 '/UserScript' $name.user.js > $name.meta.js
done
